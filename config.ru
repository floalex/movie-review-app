# This file is used by Rack-based servers to start the application.
# https://still-shelf-5875.herokuapp.com/ 
require ::File.expand_path('../config/environment',  __FILE__)
run Rails.application
