class ReviewsController < ApplicationController
  before_action :require_signin
  before_action :set_movie
  
  def index
    @review = @movie.reviews
  end
  
  def new
    @review = @movie.reviews.new
  end
  
  def create
    @review = @movie.reviews.new(review_params)
    @review.user = current_user
    if @review.save
      redirect_to movie_reviews_path(@movie), notice: "Thanks for your new review"
    else
      render 'new'
    end
  end
  
  def destroy
    @review = @movie.reviews.find(params[:id])
    @review.destroy
      redirect_to movie_reviews_path(@movie), notice: "Review has been deleted."
  end
  
  def edit
    @review = @movie.reviews.find(params[:id])
  end
  
  def update
    @review = @movie.reviews.find(params[:id])
    if @reivew.update_attributes(review_params)
      redirect_to movie_reviews_path(@movie), notice: "Thanks for your review update."
    else
      render 'edit'
    end
  end
  
  
  private
    def review_params
      params.require(:review).permit(:comment, :stars)
    end
    
    def set_movie
      @movie = Movie.find_by!(slug: params[:movie_id])
    end
end
