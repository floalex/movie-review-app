class User < ActiveRecord::Base
  has_many :reviews, dependent: :destroy
  has_many :favorites, dependent: :destroy
  has_many :favorite_movies, through: :favorites, source: :movie
  
  has_secure_password
  validates :name,  presence: true
  validates :email, presence: true, length: { minimum: 6, allow_blank: true },
                    format: /\A\S+@\S+\Z/,
                    uniqueness: { case_sensitive: false }

  def self.authenticate(email, password)
    user = User.find_by(email: email)
    user && user.authenticate(password)
  end
end
