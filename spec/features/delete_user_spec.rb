require 'spec_helper'

describe "Deleting a user" do 
  
  before do
    admin = User.create!(user_attributes(admin: true))
    sign_in(admin)
  end
  
  it "destroys the user and shows the user listing without the deleted user" do
    user = User.create!(user_attributes(email: "deleteduser@example.com"))
  
    visit user_path(user)
    
    click_link 'Delete Account'
    
    expect(current_path).to eq(root_path)
    expect(page).to have_text("Account successfully deleted!")
 
  end
  
 
 
end