require 'spec_helper'

describe "Creating a new user" do
  it "saves the user and shows the new user's profile page" do
    visit root_url

    click_link 'Sign up'

    expect(current_path).to eq(signup_path)

    fill_in "Name", with: "Example user"
    fill_in "Email", with: "user@example.com"
    fill_in "Password", with: "secrete"
    fill_in "Password confirmation", with: "secrete"
            
    click_button 'Create new account'

    expect(current_path).to eq(user_path(User.last))

    expect(page).to have_text('Example user')
    expect(page).to have_text('Thanks for signing up')
    expect(page).not_to have_link("Sign up")
    expect(page).not_to have_link("Sign In")
  end
  
  it "doesn't save the user if it's invalid" do
    visit signup_url
    
    expect { click_button 'Create new account' }.not_to change(User, :count)
    expect(page).to have_text('error')
  end
end
